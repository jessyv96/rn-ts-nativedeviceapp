import React, { useState, useEffect, useCallback } from 'react'
import { Alert, Platform, StyleSheet, Text, View } from 'react-native'
import MapView, { Marker } from 'react-native-maps';
import { HeaderButtons, Item } from 'react-navigation-header-buttons';
import CustomHeaderButton from '../components/HeaderButton';

const MapScreen = ({ navigation }) => {
    const [selectedLocation, setSelectedLocation]: any = useState();
    let markersCoordinate;
    const region = {
        latitude: 37.78,
        longitude:  -122.44,
        latitudeDelta: 0.0092,
        longitudeDelta: 0.00421,
    }

    if (selectedLocation) {
        markersCoordinate = {
            latitude: selectedLocation.lat,
            longitude: selectedLocation.lng
        }
    }

    const selectLocationHandler = (event) => {
        setSelectedLocation({
            lat: event.nativeEvent.coordinate.latitude,
            lng: event.nativeEvent.coordinate.longitude
        })
    }

    const saveLocationHandler = useCallback(() => {

        if (!selectedLocation) {
            Alert.alert('Vous devez choisir une localisation')
            return;
        }
        navigation.navigate('NewPlace', { selectedLocation: selectedLocation })
    }, [selectedLocation])

    useEffect(() => {
        navigation.setParams({ saveLocation: saveLocationHandler })
    }, [saveLocationHandler])

    return <MapView
        mapType={Platform.OS == "android" ? "none" : "standard"}
        region={region} style={styles.map}
        onPress={selectLocationHandler}>
        {selectedLocation &&
            <Marker
                coordinate={markersCoordinate}
                title='Localisation choisie'></Marker>
        }
    </MapView>
}

MapScreen.navigationOptions = (navData: any) => {
    const save = navData.navigation.getParam('saveLocation')


    return {
        title: "Carte",
        headerRight: () => <HeaderButtons HeaderButtonComponent={CustomHeaderButton}>
            <Item
                title='Save Location'
                iconName={Platform.OS === "android" ? 'md-save' : 'ios-save'}
                onPress={save} />
        </HeaderButtons>
    }
}


export default MapScreen

const styles = StyleSheet.create({
    map: {
        flex: 1
    }
})
