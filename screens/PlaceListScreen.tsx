import React, { useEffect } from 'react'
import { Platform, StyleSheet } from 'react-native'
import { FlatList } from 'react-native-gesture-handler'
import { HeaderButtons, Item } from 'react-navigation-header-buttons'
import { RootStateOrAny, useDispatch, useSelector } from 'react-redux'
import CustomHeaderButton from '../components/HeaderButton'
import PlaceItem from '../components/PlaceItem'
import { getPlaces } from '../store/place.actions'
import { IPlace, IPlaces } from '../types/Place.d'


const PlaceListScreen = ({ navigation }: any) => {
    const dispatch = useDispatch()
    const places = useSelector((state: RootStateOrAny) => state.places.places)

    useEffect(() => {
        dispatch(getPlaces())
    }, [dispatch, getPlaces])

    return <FlatList
        data={places}
        renderItem={({ item }:any) =>
            <PlaceItem
                placeId={item.id}
                address={item.address}
                imageUrl={item.imageUrl}
                title={item.title}
                onSelect={() => navigation.navigate('PlaceDetail', { title: item.title })}
            />
        }

    />
}

PlaceListScreen.navigationOptions = (navData: any) => {
    return {
        title: "Lieux",
        headerRight: () =>
            <HeaderButtons HeaderButtonComponent={CustomHeaderButton}>
                <Item
                    title='Add Place'
                    iconName={Platform.OS === "android" ? 'md-add' : 'ios-add'}
                    onPress={() => navData.navigation.navigate('NewPlace')} />
            </HeaderButtons>
    }
}

export default PlaceListScreen

const styles = StyleSheet.create({})
