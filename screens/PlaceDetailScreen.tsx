import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

const PlaceDetailScreen = () => {
    return (
        <View>
            <Text>place detail</Text>
        </View>
    )
}

PlaceDetailScreen.navigationOptions = (navData: any) => {
    return {
        title: navData.navigation.getParam('title')
    }
}


export default PlaceDetailScreen

const styles = StyleSheet.create({})
