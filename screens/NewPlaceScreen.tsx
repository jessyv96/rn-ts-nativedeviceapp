import React, { useState, useCallback } from 'react'
import { StyleSheet, Text, View, ScrollView, Button } from 'react-native'
import { TextInput } from 'react-native-gesture-handler'
import { useDispatch } from 'react-redux'
import ImgPicker from '../components/ImagePicker'
import LocationPicker from '../components/LocationPicker'
import { Colors } from '../constants/Colors'
import { addPlace } from '../store/place.actions'

const NewPlaceScreen = ({ navigation }: any) => {
    const dispatch = useDispatch()
    const [title, setTitle] = useState('')
    const [imageTaken, setImageTaken] = useState('')
    const [selectedLocation, setSelectedLocation]: any = useState();
    const setTitleHandler = (text: string): void => {
        setTitle(text)
    }

    const imageTakenHandler = (image: string) => {
        setImageTaken(image)
    }

    const locationPickedHandler = useCallback((location) => {
        setSelectedLocation(location);
    }, [])

    const savePlaceHandler = () => {
        dispatch(addPlace(title, imageTaken, selectedLocation))
        navigation.goBack()
    }

    return (
        <ScrollView>
            <View style={styles.form}>
                <Text style={styles.label}>Titre</Text>
                <TextInput
                    style={styles.textInput}
                    value={title}
                    onChangeText={setTitleHandler} />
                <ImgPicker onImageTaken={imageTakenHandler} />
                <LocationPicker navigation={navigation} onpickedLocation={locationPickedHandler} />
                <Button
                    onPress={savePlaceHandler}
                    color={Colors.Primary}
                    title='Ajouter' />
            </View>
        </ScrollView>
    )
}


NewPlaceScreen.navigationOptions = (navData: any) => {
    return {
        title: "Ajouter un lieu",
    }
}

export default NewPlaceScreen

const styles = StyleSheet.create({
    form: {
        margin: 30
    },
    label: {
        fontSize: 18,
        marginBottom: 15

    },
    textInput: {
        borderBottomColor: '#ccc',
        borderBottomWidth: 1,
        marginBottom: 15
    },
})