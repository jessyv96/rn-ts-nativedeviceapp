import { IPlace } from "../types/Place.d"

export class Place implements IPlace {
    id: String
    title: String
    imageUrl: String
    address: String
    latitude: Number
    longitude: Number

    constructor(id: String, title: String, imageUrl: String, address: String, latitude: Number , longitude: Number) {
        this.id = id
        this.title = title
        this.imageUrl = imageUrl
        this.address = address
        this.latitude = latitude
        this.longitude = longitude
    }

    [x: string]: any

    onSelect?(): void {
        throw new Error("Method not implemented")
    }

}