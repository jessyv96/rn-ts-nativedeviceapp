import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import PlaceListScreen from '../screens/PlaceListScreen';
import PlaceDetailScreen from '../screens/PlaceDetailScreen';
import NewPlaceScreen from '../screens/NewPlaceScreen';
import MapScreen from '../screens/MapScreen';
import { Platform } from 'react-native';
import { Colors } from '../constants/Colors';

const PlaceStackNavigator = createStackNavigator({
    PlaceList: PlaceListScreen,
    PlaceDetail: PlaceDetailScreen,
    NewPlace: NewPlaceScreen,
    Map: MapScreen
}, {
    defaultNavigationOptions: {
        headerStyle: {
            backgroundColor: Platform.OS === "android" ? Colors.Primary : ''
        },
        headerTintColor: Platform.OS === "android" ? 'white' : Colors.Primary
    }
})


export default createAppContainer(PlaceStackNavigator)