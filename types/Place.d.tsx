import { ADD_PLACE } from "../store/place.actions";

export interface IAddPlace {
    title: string
    setTitle(): void 
}

export interface IPlace {
    [x: string]: any;
    id?: String
    imageUrl: String
    title: String
    address: String
    lat?: Number
    lng?: Number
    onSelect?(): void
}

export interface IPlaces {
    places: IPlace[]
}

export interface IPlaceAction {
    type: typeof ADD_PLACE
    payload: IPlace
}