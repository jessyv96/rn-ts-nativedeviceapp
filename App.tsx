import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Provider } from 'react-redux'
import { Store, createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from "redux-thunk"
import * as Font from 'expo-font';
import PlaceNavigator from './navigation/PlaceNavigator';
import { placesReducer } from './store/place.reducer'
import { initDb } from './helpers/db';

initDb()
  .then(() => {
    console.log('Initialize database')
  })
  .catch((err) => {
    console.log('Initialize failed')
    throw new Error(err)
  })

export default function App() {

  const [loaded] = Font.useFonts({
    openSans: require('./assets/fonts/OpenSans-Regular.ttf'),
    openSansBold: require('./assets/fonts/OpenSans-Bold.ttf'),
  });

  if (!loaded) {
    return null;
  }
  const mainReducer = combineReducers({
    places: placesReducer
  })

  const store: Store = createStore(mainReducer, applyMiddleware(thunk))

  return <Provider store={store}>
    <PlaceNavigator />
  </Provider>

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
