import * as SQLite from 'expo-sqlite';

const db = SQLite.openDatabase('places.db') // créer la db

export const initDb: Function = () => {
    const promise: Promise<any> = new Promise((resolve: any, reject: any) => {
        db.transaction((t) => {
            t.executeSql(
                'CREATE TABLE IF NOT EXISTS places (id INTEGER PRIMARY KEY NOT NULL, title TEXT NOT NULL, imageUrl TEXT NOT NULL, address TEXT NOT NULL, lat REAL NOT NULL, lng REAL NOT NULL);',
                [],
                (_, result) => {
                    resolve(result)
                },
                (_, err) => {
                    reject(err)
                    return false
                }
            )
        })
    })
    return promise
}

export const insertPlace = (title: String, image: String, address: String, lat: Number, lng: Number) => {
    const promise: Promise<any> = new Promise((resolve: any, reject: any) => {
        db.transaction((t) => {
            t.executeSql(
                `INSERT INTO places (title, imageUrl, address, lat, lng) VALUES (?,?,?,?,?);`,
                [title, image, address, lat, lng],
                (_, result) => {
                    resolve(result)
                },
                (_, err) => {
                    reject(err)
                    return false
                }
            )
        })
    })
    return promise
}

export const fetchPlaces =  () => {
    const promise: Promise<any> = new Promise((resolve: any, reject: any) => {
        db.transaction((t) => {
            t.executeSql(
              'SELECT * FROM places',
                [],
                (_, result) => {
                    resolve(result)
                },
                (_, err) => {
                    reject(err)
                    return false
                }
            )
        })
    })
    return promise
}

export const deletePlace =  (id) => {
    const promise: Promise<any> = new Promise((resolve: any, reject: any) => {
        db.transaction((t) => {
            t.executeSql(
              'DELETE FROM places WHERE id = ?',
                [id],
                (_, result) => {
                    resolve(result)
                },
                (_, err) => {
                    reject(err)
                    return false
                }
            )
        })
    })
    return promise
}