import React from 'react'
import { Platform } from 'react-native'
import { HeaderButton } from 'react-navigation-header-buttons'
import { Ionicons } from '@expo/vector-icons';
import { Colors } from '../constants/Colors';

const CustomHeaderButton = (props: any) => {
    return (
        <HeaderButton
            {...props}
            title='save'
            IconComponent={Ionicons}
            iconSize={25}
            color={Platform.OS === 'android' ? 'white' : Colors.Primary} />

    )
}

export default CustomHeaderButton
