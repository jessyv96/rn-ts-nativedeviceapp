import React from 'react'
import { StyleSheet, Text, View, Image, Alert } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { Ionicons } from '@expo/vector-icons'
import { Colors } from '../constants/Colors'
import { useDispatch } from 'react-redux'
import { deletePlaceItem } from '../store/place.actions'

const PlaceItem = ({ placeId, title, imageUrl, address, onSelect }) => {
    const dispatch = useDispatch()
    const deleteItemHandler = () => {
        Alert.alert(
            "Supprimer un lieu",
            "Souhaitez-vous vraiment supprimer ce lieu ?",
            [
                {
                    text: "Non",
                    style: "cancel"
                },
                { text: "Oui, je le veux", onPress: () => {
                  dispatch(deletePlaceItem(placeId))
                }}
            ],
            { cancelable: false }
        );
    }
    return (
        <TouchableOpacity onPress={onSelect} style={styles.placeItem}>
            <Image style={styles.image} source={{ uri: imageUrl }} />
            <View style={styles.infoContainer}>
                <Text style={styles.title}>{title}</Text>
                <Text style={styles.address}>{address}</Text>
            </View>
            <View>
                <TouchableOpacity onPress={deleteItemHandler}>
                    <Ionicons
                        name="trash"
                        size={30}
                        color="red" />
                </TouchableOpacity>
            </View>
        </TouchableOpacity>
    )
}

export default PlaceItem

const styles = StyleSheet.create({
    placeItem: {
        borderBottomColor: '#ccc',
        borderBottomWidth: 1,
        paddingVertical: 15,
        paddingHorizontal: 30,
        flexDirection: 'row',
        alignItems: 'center'
    },
    image: {
        width: 70,
        height: 70,
        borderRadius: 35,
        backgroundColor: '#ccc',
        borderColor: Colors.Primary,
        borderWidth: 1
    },
    infoContainer: {
        marginLeft: 25,
        width: '65%',
        justifyContent: 'center',
        alignItems: 'flex-start'
    },
    title: {
        color: 'black',
        fontSize: 18,
        marginBottom: 5
    },
    address: {
        color: '#666',
        fontSize: 16
    },
})
