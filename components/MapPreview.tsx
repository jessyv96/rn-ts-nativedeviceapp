import React from 'react'
import { ActivityIndicator, Platform, StyleSheet, Text, View } from 'react-native'
import MapView from 'react-native-maps';
import { Marker } from 'react-native-maps';
import { Colors } from '../constants/Colors';


const MapPreview = ({ isLoading, location, onPick }) => {
    const region = {
        latitude:  32,
        longitude: 5.44,
        latitudeDelta: 0.0092,
        longitudeDelta: 0.00421,
    }
    return (
        isLoading ? <ActivityIndicator size='large' color={Colors.Primary} />
            : (!location ? <Text>Pas de localisation choisi</Text>
                : <MapView
                    pointerEvents="none"
                    mapType={Platform.OS == "android" ? "none" : "standard"}
                    region={region} style={styles.map} >
                    <Marker
                        coordinate={{ latitude: 32, longitude: 5.44 }}
                        title='test'
                        description='description'
                    />
                </MapView>)
    )
}

export default MapPreview

const styles = StyleSheet.create({
    map: {
        width: '100%',
        height: '100%',
    }
})
