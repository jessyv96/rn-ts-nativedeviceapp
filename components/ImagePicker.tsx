import React, { useState } from 'react'
import { StyleSheet, Text, View, Button, Image, Alert, Platform } from 'react-native'
import * as ImagePicker from 'expo-image-picker';
import { Colors } from '../constants/Colors'

const ImgPicker = ({ onImageTaken }: any) => {
    const [image, setImage] = useState();
    const checkPermission = async (): Promise<boolean> => {
        if (Platform.OS !== 'web') {
            const { status } = await ImagePicker.requestCameraPermissionsAsync();
            if (status !== 'granted') {
                Alert.alert('Sorry, we need camera roll permissions to make this work!');
                return false
            }
            return true
        }
    }

    const takeImageHandler = async () => {
        const hasPermission = await checkPermission()
        if (!hasPermission) {
            return
        }
        const image: any = await ImagePicker.launchCameraAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.All,
            allowsEditing: true,
            aspect: [16, 9],
            quality: 0.5
        });

        if (!image.cancelled) {
            setImage(image.uri);
            onImageTaken(image.uri)
        }
    }

    return (
        <View style={styles.imagePicker}>
            <View style={styles.imagePreview}>
                {!image ? <Text>No image picked yet</Text>
                    : <Image style={styles.image} source={{ uri: image }} />
                }
            </View>
            <Button
                title='Prendre une photo'
                color={Colors.Primary}
                onPress={takeImageHandler} />
        </View>
    )
}

export default ImgPicker

const styles = StyleSheet.create({
    imagePicker: {
        alignItems: "center"
    },
    imagePreview: {
        width: '100%',
        height: 200,
        marginBottom: 15,
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: "#ccc"
    },
    image: {
        width: '100%',
        height: 200
    }
})
