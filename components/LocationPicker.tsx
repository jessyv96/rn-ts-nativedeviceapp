import React, { useEffect, useState, useCallback } from 'react'
import { StyleSheet, Text, View, ActivityIndicator, Alert, Button } from 'react-native'
import { Colors } from '../constants/Colors'
import * as Location from 'expo-location';
import MapPreview from './MapPreview';


const LocationPicker = ({ navigation, onpickedLocation }) => {
    const [location, setLocation] = useState(null);
    const [isLoading, setIsLoading] = useState(false)

    const pickedLocation = navigation.getParam('selectedLocation')

    useEffect(() => {
        if (pickedLocation) {
            setLocation(pickedLocation)
            onpickedLocation(pickedLocation)
        }
    }, [pickedLocation, onpickedLocation]);

    const checkPermission = async (): Promise<boolean> => {
        const { status } = await Location.requestPermissionsAsync();
        if (status !== 'granted') {
            Alert.alert('Sorry, we need camera roll permissions to make this work!');
            return false
        }
        return true

    }
    const getLocationHandler = async () => {
        const hasPermission = await checkPermission()
        if (!hasPermission) {
            return
        }
        try {
            setIsLoading(true)
            const location = await Location.getCurrentPositionAsync()
            setLocation({
                latitude: location.coords.latitude,
                longitude: location.coords.longitude
            })
            onpickedLocation({
                latitude: location.coords.latitude,
                longitude: location.coords.longitude
            })

        } catch (error) {
            Alert.alert('Echec localisation');
        }
        setIsLoading(false)


    }
    const pickOnMapHandler = () => {
        navigation.navigate('Map')
    }

    return (
        <View style={styles.location}>
            <View style={styles.mapPreview}>
                <MapPreview
                    isLoading={isLoading}
                    location={location}
                    onPick={pickOnMapHandler}
                />
            </View>
            <View style={styles.actions}>
                <Button title='Localisation' color={Colors.Primary} onPress={getLocationHandler} />
                <Button title='Selectionner Sur la carte' color={Colors.Primary} onPress={pickOnMapHandler} />
            </View>
        </View>
    )
}

export default LocationPicker

const styles = StyleSheet.create({
    location: {
        marginBottom: 20,
        alignItems: "center"
    },
    mapPreview: {
        marginBottom: 15,
        width: '100%',
        height: 200,
        borderColor: "#ccc",
        borderWidth: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    actions: {
        flexDirection: "row",
        justifyContent: "center",
    }

})