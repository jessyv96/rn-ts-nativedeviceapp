import * as FileSystem from 'expo-file-system';
import * as Location from 'expo-location';
import { insertPlace, fetchPlaces, deletePlace } from '../helpers/db'

export const ADD_PLACE: string = 'ADD_PLACE'
export const DELETE_PLACE: string = 'DELETE_PLACE'
export const GET_PLACES: string = 'GET_PLACES'

export const addPlace = (title: string, image: string, location) => async (dispatch: any) => {
    const filename: string = image.split('/').pop()
    const newPath: string = FileSystem.documentDirectory + filename
    const geocodeAddress = await Location.reverseGeocodeAsync(location)
    const address = {
        street: geocodeAddress[0].name,
        postalCode: geocodeAddress[0].postalCode,
        city: geocodeAddress[0].city,
        country: geocodeAddress[0].country,
    }
    const address_formatted: string = `${address.street}, ${address.postalCode} ${address.city}, ${address.country}`

    try {
        await FileSystem.moveAsync({
            from: image,
            to: newPath
        })
        const dbResult = await insertPlace(title, newPath, address_formatted, location.latitude, location.longitude)

        return dispatch({
            type: ADD_PLACE,
            payload: {
                id: dbResult.insertId.toString(),
                title,
                image: newPath,
                location,
                address: address_formatted
            }
        })
    } catch (error) {
        throw new Error(error)
    }

}

export const getPlaces = () => async (dispatch: any) => {
    try {
        const dbResult = await fetchPlaces()

        return dispatch({
            type: GET_PLACES,
            payload: dbResult.rows._array
        })
    } catch (error) {
        throw new Error(error)
    }
}

export const deletePlaceItem = (id) => async (dispatch: any) => {
    try {
        await deletePlace(id)

        return dispatch({
            type: DELETE_PLACE,
        })

    } catch (error) {
        throw new Error(error)
    }

}