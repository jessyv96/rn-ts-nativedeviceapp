import { Place } from "../models/Place";
import { IPlace, IPlaceAction, IPlaces } from "../types/Place.d";
import { ADD_PLACE, DELETE_PLACE, GET_PLACES } from "./place.actions"

const initialState: IPlaces = {
    places: []
}

export const placesReducer = (state = initialState, action: IPlaceAction) => {
    switch (action.type) {
        case GET_PLACES:
            return {
                ...state,
                places: action.payload
            }
        case ADD_PLACE:
            const newPlace: IPlace = new Place(
                action.payload.id.toString(),
                action.payload.title,
                action.payload.image,
                action.payload.address,
                action.payload.location.latitude,
                action.payload.location.longitude
            )

            return {
                ...state,
                places: state.places.concat(newPlace)
            };
        case DELETE_PLACE:
            return {
                ...state,
                places: state.places
            };
        default:
            return state;
    }
}